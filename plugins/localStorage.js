import createPersistedState from 'vuex-persistedstate'

export default ({ store, req, isDev }) => {
  createPersistedState({
    key: 'babyprojectforimkuo',  //自己設
    paths: ['user', 'classes', 'students', 'student', 'currentCID', 'currentStudent', 'currentClass','measuretime', 'feedback', 'feedback2', 'child_id', 'feedback_ans', 'classStudents', 'filenames', 'k', 'stdata','userlist'], //user要永存直到按下登出
  })(store)
}

