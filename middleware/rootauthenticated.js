export default async function({ store, redirect }) {
  if (!store.state.user) {
    return redirect("/login")
  }
  if (store.state.user.role !== "root") {
    return redirect("/classes")
  }

}
