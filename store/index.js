import {
  fireDb,
  fireAuth
} from "@/plugins/firebase.js";
// import { state } from "fs";


export const state = {
  user: null,
  userlist:null,
  records: null, //資料庫裡裡全部訪談資料
  record: null,
  stdatas: null, //陣列
  currentStudent: null, //物件
  currentStudent2: null,//alumni
  UID: null,
  measure: null,  //畫評量表圖表用的 chart.js
  measuretime: null, //紀錄第幾次填寫評量
  measureform: null,//從firebase抓評量表資料放到這 //好像用不到了
  vform_analyze: null,
  currentClass: null,
  currentCID: null,
  classes: null,
  students: null,
  feedback_currentClass: null,
  feedback: null,//parent_feedback mounted抓下來的值
  feedback2: null,
  child_id: null, //feedback_list裡用到
  date: null,//feedback_list裡用到
  feedback_ans: null,//feedback_ans裡用到
  tracking: null,
  filenames: [],//assess裡用到
  k: "",//develop_pic,
  anastudent:"",//ClassAnalysis.vue用
  // DateButtons:[],//visit_revord.vue用

};

export const mutations = {
  setObject(state, data) {   //舊的資料會被蓋掉
    state[data.key] = data.value
  },
  addItem(state, data) {  //補一筆新資料 只能陣列形式
    if (state[data.key]) {
      state[data.key].push(data.value)
    } else {
      state[data.key] = [data.value]
    }
  },
  concatItems(state, data) {
    state[data.key] = state[data.key].concat(data.value)
  },
  setField(state, data) {
    state[data.obj][data.field] = data.value
  },
  setStdataRecords(state, data) {//存訪談紀錄
    state.currentStudent.records = data
  },
  logout(state) {//登出清空永久儲存資料
    state.user = null
    state.classes = null
    state.students = null
    state.currentCID = null
    state.dataprofile = null
  },
  addClassStudent(state, data) {//新增某一個班級的學生
    if (state.classes) {
      for (let i = 0; i < state.classes.length; i++) {
        if (state.classes[i].id == state.currentClass.id) {
          if (state.classes[i].students) {
            state.classes[i].students.push(data)
          } else {
            cls.students = [data]
            state.classes = cls
          }
          return
        }
      }
    }
  },
  changeStdClassRemove(state) {

    for (let i = 0; i < state.classes.length; i++) {
      for (let j = 0; j < state.classes[i].students.length; j++) {
        if (state.currentClass.id == state.classes[i].id) {
          if (state.classes[i].students[j] === state.currentStudent.id) {
            state.classes[i].students.splice(j, 1);
            j--;
          }
          if (state.currentClass.students[j] === state.currentStudent.id) {
            state.currentClass.students.splice(j, 1);
            j--;
            // console.log("NewCurrentClass", state.currentClass)
          }

        }
      }
    }
  },
  changeStdClassAdd(state, classid) {
    for (let i = 0; i < state.classes.length; i++) {
      if (classid == state.classes[i].id) {
        if (state.classes[i].students) {
          state.classes[i].students.push(state.currentStudent.id)
        } else {
          stateclasses[i].students = [state.currentStudent.id]

        }


      }
    }
  },
  changStdStatus(state, data) {
    // console.log("Data, Status=", data.key, data.value)
    if (state.students) {
      for (let i = 0; i < state.students.length; i++) {
        if (state.students[i].id == data.key) {
          // console.log("Data, Status=", data.key, data.value)
          state.students[i].status = data.value
          break
        }

      }
    }
  },
  // changeUserRole(state,data){
  //   if(state.user){
  //     for(let i=0;i<;i++){
  //       if(state.user[i].id==data.key){
  //         state.user[i].role = data.value
  //       }
  //     }
  //   }
  // }
};

export const actions = {
  async getCollection({
    commit
  }, collname) {
    await fireDb.collection(collname).get().then(records => {
      // console.log("records", records)
      // console.log("collname", collname)
      if (!records.empty) {
        let result = []
        records.forEach(record => {
          let p = record.data()
          p.id = record.id
          result.push(p)
        })
        commit("setObject", { key: collname, value: result })
      }
    })
  },
  async signInWithEmail({
    commit, dispatch
  }, payload) {
    //console.log("Payload", payload)
    await fireAuth.signInWithEmailAndPassword(payload.email, payload.password)
    let cuser = fireAuth.currentUser;
    //console.log("Cuser", cuser)
    await fireDb.collection("user").doc(cuser.uid).get().then(ans => {
      // console.log("ANS", ans)
      let p = ans.data()
      p.id = cuser.uid
      commit("setObject", {
        key: "user",
        value: p
      });
    })
    await dispatch("getCollection", "classes")
    await dispatch("getCollection", "students")
    await dispatch("getCollection", "feedback")
    await dispatch("getCollection", "feedback2")

  },

  async signUp({ commit }, payload) {
    //console.log("call signUp sucess");
    // console.log("index payload", payload)
    let cuser = null
    await fireAuth
      .createUserWithEmailAndPassword(payload.account, payload.password)
      .then(user => {
        console.log("Your account has been created!");
        cuser = fireAuth.currentUser;
        console.log("cuser=fireAuth.currentUser", cuser)
      });
    if (cuser) {
      await fireDb
        .collection("user")
        .doc(cuser.uid)
        .set(payload)
        .then(res => {
          let udata = payload
          udata.id = cuser.uid
          // console.log("cuser.uid", cuser.uid)
          commit("setObject", {
            key: "user",
            value: udata
          });
        });
    }

  },

  signOut({
    commit
  }) { //Navbar 用的
    return fireAuth.signOut()
      .then(() => {
        // eslint-disable-next-line no-console
        console.log("User Signout");
        commit("logout")
      });
  }
};

export const getters = {
  getCurrentClass: state => {
    for (let i = 0; i < state.classes.length; i++) {
      if (state.classes[i].id == state.currentCID) {
        console.log("state.classes[i]", state.classes[i])
        return state.classes[i]
      }
    }
    return null
  },
};


